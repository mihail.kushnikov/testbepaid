export function asyncHerosOnPage(herosOnPage, page){
    return dispatch =>{
        dispatch({type:'LOAD_HEROS', payload: {herosOnPage:herosOnPage, page:page}})
    }
}


export function asyncGetCurrentHero(currentHeroInfo){
    return dispatch => {
        dispatch({type:'GET_HERO_INFO', payload: {currentHeroInfo: currentHeroInfo}})
    }
}
