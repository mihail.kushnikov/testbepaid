import React, {Component} from 'react'
import LazyLoad from 'react-lazyload';
import { connect } from 'react-redux';
import {withRouter } from 'react-router-dom';
import { asyncGetCurrentHero } from '../../Redux/userReduxActions';
import Placeholder from '../spiner';
import styled from 'styled-components';

const Cart = styled.div`
    width: 300px;
    height: 350px;
    margin: 20px;
    border-radius: 5px;
    box-shadow: 0px 3px 9px rgba(19, 72, 252, 0.1);
    &:hover {box-shadow: 0px 8px 28px rgba(19, 72, 252, 0.1)};
`;

const HeroName = styled.span`
    font-size: 16px
    display: inline-flex;
    align-items: center;
    justify-content: center;
    line-height: 50px;
    font-weight: 600;
`;


class HeroCart extends Component{
    constructor(props){
        super(props)
    }

    redirectToHero(heroInfo){
        console.log(heroInfo)
        this.props.makeDispathc(heroInfo)
        this.props.history.push(`/allheros/currenthero/${heroInfo.id}`)
    }

    render(){
        return(
            <Cart>
                <LazyLoad height={300} className="hero" style={{width:300}}  debounce={700} offset={[-200, 0]} placeholder={<Placeholder />}>
                    <div onClick={()=>this.redirectToHero(this.props.fullInfo)} style={{cursor:'pointer'}}>
                        <img style={{borderRadius: 5}} src={this.props.image} key={this.props.id} className="heroImage" />
                        <div className='heroName'>
                            <HeroName>{this.props.name}</HeroName>
                        </div>
                    </div>
                </LazyLoad>
            </Cart>
        )
    }
}

function mapStateToProps(state){
    return {
        state: state
    }
}

export default withRouter(connect(mapStateToProps,
    dispathc => ({
        makeDispathc: (info) => {
            dispathc(asyncGetCurrentHero(info))
        }
    })
    )(HeroCart))