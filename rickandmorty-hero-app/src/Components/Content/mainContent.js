import React, { Suspense, lazy } from 'react'
import { Route, Switch } from 'react-router-dom'
import AllHeros from '../AllHeros/allHeros'
import CurrentHero from '../CurrentHero/currentHero'

const Content = () => {
    return(
        <Suspense fallback='Loading'>
            <Switch>
                <Route exact path='/' component={AllHeros} />
                <Route exact path='/allheros/:id' component={AllHeros} />
                <Route exact path='/allheros/currenthero/:id' component={CurrentHero} />
                <Route exact path='*' component={AllHeros} />
            </Switch>
        </Suspense>
    )
}

export default Content