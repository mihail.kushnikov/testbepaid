import axios from 'axios'

export class AllApi {
    axiosInstance;

    constructor(){
        this.axiosInstance = axios.create({
            baseURL: this.baseUrl()
        });


    }

    baseUrl(){
        return 'https://rickandmortyapi.com/api'
    }


    MakeGetCall(url){
        return this.axiosInstance.get(url).then(res=>{
            return res
        })
    }
}
let api = new AllApi()

export default api;