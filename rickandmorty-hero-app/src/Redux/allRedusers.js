import {combineReducers} from 'redux'
import { allHerosOnPage, getCurrentHero } from './action'

const allReducers = combineReducers({
    allHerosOnPage: allHerosOnPage,
    getCurrentHero: getCurrentHero
})

export default allReducers