import React, {Component} from 'react'
import { connect } from 'react-redux'
import api from '../../Api/allApi'
import { asyncGetCurrentHero } from '../../Redux/userReduxActions'
import styled from 'styled-components';

const CurrentHeroCart = styled.div`
    width: 300px;
    padding-bottom: 8px;
    border-radius: 5px;
    box-shadow: 0px 3px 9px rgba(19, 72, 252, 0.1);
    margin: 50px auto;
    `;

const GoBack = styled.a`
    text-decoration: none;
    cursor: pointer;
    margin-top: 10px;
`;


class CurrentHero extends Component {
    constructor(props){
        super(props)
        this.state= {
            name: '',
            status: '',
            lastLocation: '',
            imageUrl: ''
        }
    }

    componentDidMount(){
        // console.log(this.props.state.getCurrentHero.heroInfo.id)
        // console.log(this.props.state.getCurrentHero.heroInfo)
        if(this.props.state.getCurrentHero.heroInfo){
            this.setState({
                name: this.props.state.getCurrentHero.heroInfo.name,
                status: this.props.state.getCurrentHero.heroInfo.status,
                lastLocation:this.props.state.getCurrentHero.heroInfo.location.name,
                imageUrl: this.props.state.getCurrentHero.heroInfo.image
            })
        } else {
            api.MakeGetCall(`/character/${this.props.match.params.id}`).then(res=>{
                console.log(res.data)
                this.props.makeDispathc(res.data)
                this.setState({
                    name: res.data.name,
                    status: res.data.status,
                    lastLocation: res.data.location.name,
                    imageUrl: res.data.image
                })
            }).catch(err=>{
                this.setState({
                    name: "Character not found",
                    imageUrl: 'https://thecreativeindustry.com/wp-content/uploads/2018/12/404.jpg'
                })
            })
        }
    }


    goToPrevPage(){
        console.log(this.props.state.allHerosOnPage.page)
        if(this.props.state.allHerosOnPage){
            this.props.history.push(`/allheros/${parseInt(this.props.state.allHerosOnPage.page, 10)}`)
        } else {
            this.props.history.push(`/allheros/${1}`)
        }
    }


    render(){
    const Status = styled.div`
        width: 5px;
        height: 5px;
        border-radius: 100%;
        margin: 0 5px;
        background: ${this.state.status === 'Alive'? '#2b9e16':(this.state.status === 'Dead' ? '#d11521' : '#87868f')}
    `;
    const InfoText = styled.div`
        display: ${this.state.name === 'Character not found' ? 'none' : 'inline-flex'};
        flex-direction: row;
        justify-content: center;
        align-items: center;
    `;
        return(
            <div style={{paddingTop: 30}}>
                <GoBack onClick={()=>this.goToPrevPage()}>Go to back</GoBack>
                <CurrentHeroCart>
                    <img style={{borderRadius: 5}} width={300} height={300} src={this.state.imageUrl} />
                    <div>
                        <p>Name: {this.state.name}</p>
                        <InfoText>Status: <Status />{this.state.status}</InfoText>
                        <p style={{display: this.state.name === 'Character not found' ? 'none' : 'block'}}>Last known location: {this.state.lastLocation}</p>
                    </div>
                </CurrentHeroCart>
            </div>
        )
    }

}

function mapStateToProps(state){
    return {
        state: state
    }
}

export default connect(mapStateToProps, 
    dispathc => ({
        makeDispathc: (info) => {
            dispathc(asyncGetCurrentHero(info))
        }
    })
    )(CurrentHero)