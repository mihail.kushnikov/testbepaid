import React, {Component} from 'react'
import { connect } from 'react-redux'
import api from '../../Api/allApi'
import { asyncHerosOnPage } from '../../Redux/userReduxActions'
import HeroCart from '../HeroCart/heroCart'
import Pagination from "react-js-pagination";
import "bootstrap/dist/css/bootstrap.min.css";
import styled from 'styled-components';


const MainContent = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
`;
const AllHeroBlock = styled.div`
    display: flex;
    flex-direction: row;
    flex-wrap: wrap;
    justify-content: space-around;
`;

class AllHeros extends Component{
    currentPage;
    constructor(){
        super()
        this.state = {
            allhero: [],
            maxCountPage: 0,
            allCount: 0,
            itemsInArray: 0
        }
    }

    componentWillMount(){
        this.currentPage = this.props.match.params.id ? this.props.match.params.id : 1
        console.log(this.currentPage)
    }

    componentDidMount(){
        api.MakeGetCall(`/character/?page=${this.currentPage}`).then(res=>{
            console.log(res.data)
            this.setState({allhero: res.data.results, maxCountPage: res.data.info.pages, allCount:res.data.info.count, itemsInArray: res.data.results.length})   
            this.props.makeDispathc(res.data.results, this.currentPage)       
        })
    }

    pageNumber(current){
        console.log(current)

        api.MakeGetCall(`/character/?page=${current}`).then(res=>{
            console.log(res.data)
            this.setState({allhero: res.data.results, maxCountPage: res.data.info.pages})   
            this.props.makeDispathc(res.data.results, current)       
            this.props.history.push(`/allheros/${current}`)
        })
    }
    render(){
        const allHerosBlock = this.state.allhero.map(x=>{
        return (<HeroCart name={x.name} key={x.id} id={x.id} image={x.image} fullInfo={x} />)
        })
        return(
            <MainContent>
                <AllHeroBlock>
                    {allHerosBlock}
                </AllHeroBlock>
                <div className="pages">
                    <nav aria-label="Page navigation">
                        <Pagination 
                            onChange={(current)=>{this.pageNumber(current)}} 
                            activePage={parseInt(this.props.state.allHerosOnPage.page, 10)} 
                            totalItemsCount={this.state.allCount} 
                            itemsCountPerPage={this.state.itemsInArray === 20? this.state.itemsInArray : 20}	
                        />
                    </nav>
                </div>
            </MainContent>
        )
    }
}
function mapStateToProps(state){
    return {
        state: state
    }
}


export default connect(mapStateToProps,
    dispathc => ({
        makeDispathc: (herosOnPage, page) => {
            dispathc(asyncHerosOnPage(herosOnPage, page))
        }
    })
    )(AllHeros)