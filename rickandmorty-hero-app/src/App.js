
import './App.css';
import Content from './Components/Content/mainContent';

function App() {
  return (
    <div className="App">
      <Content />
    </div>
  );
}

export default App;
