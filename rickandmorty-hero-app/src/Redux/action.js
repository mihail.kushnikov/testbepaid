export function allHerosOnPage(state ='', action){
    // console.log(state)
    if(action.type === 'LOAD_HEROS'){
        return {
            herosOnPage: action.payload.herosOnPage,
            page:action.payload.page
        }
    } else {
        return state
    }
}
export function getCurrentHero(state='', action){
    if(action.type === 'GET_HERO_INFO'){
        return {
            heroInfo: action.payload.currentHeroInfo
        }
    } else {
        return state
    }
    
}